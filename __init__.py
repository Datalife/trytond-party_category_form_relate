# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .category import CategoryOpenParty, CategoryOpenPartyStart


def register():
    Pool.register(
        CategoryOpenPartyStart,
        module='party_category_form_relate', type_='model')
    Pool.register(
        CategoryOpenParty,
        module='party_category_form_relate', type_='wizard')
