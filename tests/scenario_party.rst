==============
Label Scenario
==============

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install label::

    >>> config = activate_modules('party_category_form_relate')

Create Categories::

    >>> Category = Model.get('party.category')
    >>> category1 = Category(name='Category 1')
    >>> category1.save()
    >>> category2 = Category(name='Category 2')
    >>> category2.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> party1 = Party(name='Party 1')
    >>> party1.categories.append(category1)
    >>> party1.save()
    >>> party2 = Party(name='Party 2')
    >>> category1 = Category(category1.id)
    >>> party2.categories.append(category1)
    >>> party2.categories.append(category2)
    >>> party2.save()

Open Wizard::

    >>> open_party = Wizard('party.category.open_party', [category1])
    >>> len(open_party.form.categories)
    1
    >>> open_party.form.mode
    'all'
    >>> open_party.execute('open_')
