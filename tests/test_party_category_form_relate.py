# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import doctest
import unittest
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import doctest_teardown, doctest_checker
from trytond.tests.test_tryton import suite as test_suite


class PartyCategoryFormRelateTestCase(ModuleTestCase):
    """Test Party Category Form Relate module"""
    module = 'party_category_form_relate'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            PartyCategoryFormRelateTestCase))
    suite.addTests(doctest.DocFileSuite(
        'scenario_party.rst',
        tearDown=doctest_teardown, encoding='utf-8',
        checker=doctest_checker,
        optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
