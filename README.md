datalife_party_category_form_relate
===================================

The party_category_form_relate module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-party_category_form_relate/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-party_category_form_relate)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
